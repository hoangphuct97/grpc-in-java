## **What is RPC?**
- In distributed system, RPC (Remote Procedure Call) is known as a protocol that one program can use to request a function call from a process to a same computer or to a remote network without having to understand the network's detail.

## **GRPC**
- An open source high performance RPC framework which is based on ProtoBuf and HTTP/2
- Handling communications in a distributed client-server architecture. It provides ways to call and execute methods of a remote service.
- Using **Protocol Buffer** as a language to define services, and a format to communicate between services.
- Key feature is transparency principle: remote instances interact so closely and smoothly, even at great distances, that their communication appears identical to local communication between internal machine processes.
![alt text](https://www.ionos.com/digitalguide/fileadmin/DigitalGuide/Screenshots_2020/diagram-of-message-exchange-using-grpc.png "GRPC architecture")

## **Protobuf**
- A language used to describe the data structure and developers will use it to define both at the server and client
- Using protoc to compile into any kinds of supported languages, and these object in protobuf can be serialized and deserialized into a binary stream.
- Compared to Xml or Json, Protobuf size is 3-10 times smaller and 20-100 times faster

## **HTTP/2 in GRPC**
- Multiplex: send and receive many results in just 1 connection
- Header compression: compress the information of request, then client and server will store the previous information, the next request can be skipped some existing information
- Binary data: send binary data instead of text form in order to make an effective transportation and avoiding some data execution exception (like blank space, special character...)

## **RPC methods**
- **Unary RPC**: A client sends a request to a server and waits the server handles that request and responses the result to client.
```proto
service SearchService {
    rpc Search (SearchRequest) returns (SearchResponse);
}
```

- **Server Streaming RPC**: A client send a request to a server then waits the server to return a stream of data. The client keeps reading messages from the stream until nothing is returned. The order of messages in streams will be ensured while transporting between the client and server.
```proto
service SearchService {
    rpc LotsOfReplies (HelloRequest) returns (stream HelloResponse);
}
```

- **Client Streaming RPC**: Just same as Server Stream RPC. Client will send stream to server and server will read that stream data and execute some logics, afterwards returns to the client.
```proto
service SearchService {
    rpc LotsOfGreetings (stream HelloRequest) returns (HelloResponse);
}
```

- **Bidirectional Streaming RPC**: Data will be sent as a stream from both sides, client and server. The stream data is independently. When the client sends a message to the server, server can handle some logics (while receiving another messages) and send back to the client (while sending another messages)
```proto
service SearchService {
    rpc LotsOfGreetings (stream HelloRequest) returns (stream HelloResponse);
}
```

## **How GRPC works**
- **Using protocol buffer to define**: Protocol buffers perform several functions in the GRPC system. They serve as an interface definition language and describe an interface in a language-independent way. They also define the services to be used, and the features provided.
- **Workflow**:
1. Defining the service contract for inter-process communication: the service to be set up
2. Generating the GRPC code from *.proto* file
3. Implementing the server
4. Creating the client stub to call the service
![alt text](https://www.ionos.com/digitalguide/fileadmin/DigitalGuide/Screenshots_2020/diagram-of-grpc-workflow.png "GRPC workflow")

- **Example**
    + First, on the server side, a service, and a GRPC server are implemented based on protocol buffers. The requesting client generates a matching stub. If the stub is available, the client application calls the corresponding function in the stub.
    + The request is then sent to the server over the network. After receiving the request via a suitable service interface, the GRPC server does the logic things.
    + The server then sends a response to the client stub, which forwards the return value to the original requesting instance

## **Stub**
- The stub layer is some things to be exposed to most developers and provides type-safe bindings to whatever datamodel/IDL(interface definition language)/interface you are adapting. GRPC comes with a plugin to the protocol buffers compiler that generates stub interfaces out of .proto file, but bindings to other datamodel/IDL are easy and encouraged.

## **Channel**
- The channel layer is an abstraction over transporting handling that is suitable for interception/decoration and exposes more behavior to the application than the stub layer. It is intended to be easy for application frameworks to use this layer to address cross-cutting concerns such as logging, monitoring, authentication,...

## **Transport**
- The transport layer does the heavy lifting of putting the taking bytes off the wire. The interfaces to it are abstract just enough to allow plugging in of different implementations.

## **Pros and Cons**
**Pros**
- Easy to implement due to its simple and easy-to-learn IDL for creating .proto file.
- Scalable and ability to be used for complex and extensive communication, such as in globally connected client-server architectures.
- Taking advantage of HTTP/2 to boost efficiency, reduce the data volume of requests and responses in the network.
- Multi-layered and highly standardized design which helps developers can focus on implementing the methods.
- Protocol buffers make GRPC easy to adapt to different operating systems, different programming languages.
**Cons**
- Testing is inadequate at the stage of developement. GRPC messages encoded with protobuf are not human-readable. When analyzing traffic or especially debugging, you have to use additional instances to convert the code into an understandable form and locate sources of error.

## **Set up GRPC in Spring**
- Step 1: Define protobuf file and run ``mvn clean install``
- Step 2: Set up pom.xml ([Link](../master/pom.xml))
- Step 3: Declaring port for Grpc
```yml
grpc:
  port: 8081
```
- Step 4: By add an annotation ``@GRpcService``, you can create a GRPC controller extending the generated proto code.
- Step 5: Implement the service and handle your logic.

## **GRPC Web**
- Similar to GRPC, GRPC Web has many advantages to support the client to communicate clearly with the server.
- GRPC Web needs a proxy server to convert the request from HTTP/1.x to HTTP/2 and forward it to the backend server.
#### **Installation**
- Install package google-protobuf and grpc-web by running: `npm install google-protobuf` and `npm install grpc-web`
- Install protoc plugin:
    + Download prtoc-gen-grpc-plugin at link: https://github.com/grpc/grpc-web/releases
    + ```shell script
      sudo mv ~/Downloads/protoc-gen-grpc-web-1.2.1-darwin-x86_64 \
          /usr/local/bin/protoc-gen-grpc-web

    + ```shell script
      chmod +x /usr/local/bin/protoc-gen-grpc-web
- In this project, we will use Envoy([Link](https://www.envoyproxy.io/)) as a proxy server to forward the request:
    + Define envoy.yaml ([Link](../master/envoy.yaml))
    + Define Dockerfile ([Link](../master/Dockerfile))
    + Build docker: `docker build -t grpc-web .`
    + Run docker image: `docker run -d -p 9090:9090 grpc-web`
    + Generate proto file to javascript: 
    ```shell script
    echo "Generating message classes"
    protoc -I=$PROTO_SOURCE_DIR base_request.proto \
      --js_out=import_style=commonjs:$GRPC_WEB_DIR
    
    echo "Generating stub"
    protoc -I=$PROTO_SOURCE_DIR base_service_interfaces.proto \
      --grpc-web_out=import_style=commonjs,mode=grpcwebtext:$GRPC_WEB_DIR
  
- Implement the generated file.

**Notes: some issue to research**
- Compare RPC with some others methodologies
- Benchmark
- Use WireShark to catch the packages and compare with REST
- GRPC security
- GRPC proxy
- Connection between server and client